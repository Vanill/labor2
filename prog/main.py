import psycopg2

def connect_to_db():
    conn = psycopg2.connect(
        database="postgres",
        user="postgres",
        password="adm",
        host="data_service",
        port="5432"
    )
    return conn

def bad_performance():
    conn = connect_to_db()
    cursor = conn.cursor()
    cursor.execute("""
	WITH bad_student AS (
	    SELECT s.student_id, s.last_name, s.first_name, AVG(g.mark) AS avg_mark
	    FROM students s
	    JOIN grades g ON s.student_id = g.student_id
	    GROUP BY s.student_id, s.last_name, s.first_name
	    ORDER BY avg_mark ASC
	    LIMIT 1
	)
	SELECT last_name, first_name
	FROM bad_student;
    """)
    result = cursor.fetchall()
    print('ФИО:', result[0][0], result[0][1])
    print()
    cursor.close()
    conn.close()
    
def get_grades():
    conn = connect_to_db()
    cursor = conn.cursor()
    cursor.execute("""
	WITH bad_student AS (
	    SELECT s.student_id, s.last_name, s.first_name, AVG(g.mark) AS avg_mark
	    FROM students s
	    JOIN grades g ON s.student_id = g.student_id
	    GROUP BY s.student_id, s.last_name, s.first_name
	    ORDER BY avg_mark ASC
	    LIMIT 1
	)
	SELECT subject_name, mark
	FROM bad_student
	JOIN grades g ON bad_student.student_id = g.student_id
	JOIN subjects s ON g.subject_id = s.subject_id;
    """)
    result = cursor.fetchall()
    
    print("| Предмет   | Оценка |")
    print("---------------------")
    
    for subject, mark in result:
        print("| {:<10} | {:<6} |".format(subject, mark))

    
    cursor.close()
    conn.close()

def main():
    bad_performance()
    get_grades()

if __name__ == "__main__":
    main()
