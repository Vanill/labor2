CREATE TABLE students (
    student_id SERIAL PRIMARY KEY,
    last_name VARCHAR(50) NOT NULL,
    first_name VARCHAR(50) NOT NULL,
    birthdate DATE NOT NULL
);


CREATE TABLE subjects (
    subject_id SERIAL PRIMARY KEY,
    subject_name VARCHAR(100) NOT NULL
);

CREATE TABLE teachers (
    teacher_id SERIAL PRIMARY KEY,
    full_name VARCHAR(100) NOT NULL
);

CREATE TABLE grades (
    grade_id SERIAL PRIMARY KEY,
    subject_id INT REFERENCES subjects(subject_id),
    student_id INT REFERENCES students(student_id),
    exam_date DATE NOT NULL,
    teacher_id INT REFERENCES teachers(teacher_id),
    mark INT NOT NULL
);


INSERT INTO teachers (full_name) VALUES
    ('Королёв Константин Денисович'),
    ('Дьячков Леонид Михайлович'),
    ('Рыбаков Михаил Эдуардович');


INSERT INTO students (last_name, first_name, birthdate) VALUES
    ('Кузнецов', 'Максим', '2002-01-01'),
    ('Комаров', 'Генадий', '2002-02-02'),
    ('Лаврентьев', 'Роман', '2002-03-03'),
    ('Виноградов', 'Андрей', '2001-04-04'),
    ('Журавлёв', 'Александр', '2003-05-05');


INSERT INTO subjects (subject_name) VALUES
    ('География'),
    ('Русский'),
    ('Физика');


INSERT INTO grades (subject_id, student_id, exam_date, teacher_id, mark) VALUES
    (1, 1, '2021-01-12', 1, 4),
    (1, 2, '2021-01-12', 1, 3),
    (1, 3, '2021-01-12', 1, 5),
    (1, 4, '2021-01-15', 2, 4),
    (1, 5, '2021-01-15', 2, 3),
    (2, 1, '2021-01-15', 2, 4),
    (2, 2, '2021-01-25', 3, 4),
    (2, 3, '2021-01-25', 3, 3),
    (2, 4, '2021-01-25', 3, 5),
    (2, 5, '2021-01-25', 3, 3),
    (3, 1, '2021-01-15', 2, 4),
    (3, 2, '2021-01-25', 3, 4),
    (3, 3, '2021-01-25', 3, 3),
    (3, 4, '2021-01-25', 3, 5),
    (3, 5, '2021-01-25', 3, 4);


